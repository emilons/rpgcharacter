﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacter.Classes
{
    /// <summary>
    /// Type of item characters can equip, can provide a bonus to a character's attributes.
    /// </summary>
    public class Armor : Item
    {
        public enum ArmorType
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }

        public ArmorType Type { get; set; }

        /// <summary>
        /// Creates an armor object.
        /// </summary>
        /// <param name="type">The armor material.</param>
        /// <param name="lvl">Level of the armor.</param>
        /// <param name="slot">The equipment slot of which the armor occupies on a character.</param>
        /// <param name="rarity">Rarity of the armor, defaulted to common.</param>
        public Armor(ArmorType type, int lvl, Slot slot, Rarity rarity = Rarity.Common)
        {
            Type = type;
            Name = $"{rarity.ToString()} {type.ToString()} {slot.ToString()}";
            Level = lvl;
            InventorySlot = slot;
            Attributes = HandleAttributes(rarity, type);
        }

        /// <summary>
        /// Helper function to the armor constructor, handles the attribute gain of rarer armor.
        /// </summary>
        /// <param name="rarity">Rarity of the armor.</param>
        /// <param name="type">The armor material.</param>
        /// <returns>A PrimaryAttributes object with the bonus attributes.</returns>
        // 
        private PrimaryAttributes HandleAttributes(Rarity rarity, ArmorType type)
        {
            PrimaryAttributes gains = new PrimaryAttributes();
            
            // If armor is common, gain no attributes
            if (rarity.Equals(Rarity.Common))
            {
                return gains;
            }

            switch(type)
            {
                case(ArmorType.Cloth):
                    switch(rarity)
                    {
                        case (Rarity.Uncommon):
                            gains += new PrimaryAttributes(0, 0, 1, 0);
                            break;
                        
                        case (Rarity.Rare):
                            gains += new PrimaryAttributes(0, 0, 2, 0);
                            break;

                        case(Rarity.Epic):
                            gains += new PrimaryAttributes(0, 0, 4, 0);
                            break;

                        case (Rarity.Legendary):
                            gains += new PrimaryAttributes(0, 0, 7, 0);
                            break;

                    }
                    break;

                case (ArmorType.Leather):
                    switch (rarity)
                    {
                        case (Rarity.Uncommon):
                            gains += new PrimaryAttributes(0, 1, 0, 0);
                            break;

                        case (Rarity.Rare):
                            gains += new PrimaryAttributes(0, 2, 0, 0);
                            break;

                        case (Rarity.Epic):
                            gains += new PrimaryAttributes(0, 4, 0, 0);
                            break;

                        case (Rarity.Legendary):
                            gains += new PrimaryAttributes(0, 7, 0, 0);
                            break;

                    }
                    break;

                case (ArmorType.Mail):
                    switch (rarity)
                    {
                        case (Rarity.Uncommon):
                            gains += new PrimaryAttributes(1, 1, 0, 0);
                            break;

                        case (Rarity.Rare):
                            gains += new PrimaryAttributes(1, 1, 0, 1);
                            break;

                        case (Rarity.Epic):
                            gains += new PrimaryAttributes(2, 2, 0, 1);
                            break;

                        case (Rarity.Legendary):
                            gains += new PrimaryAttributes(3, 3, 0, 1);
                            break;

                    }
                    break;

                case (ArmorType.Plate):
                    switch (rarity)
                    {
                        case (Rarity.Uncommon):
                            gains += new PrimaryAttributes(1, 0, 0, 1);
                            break;

                        case (Rarity.Rare):
                            gains += new PrimaryAttributes(2, 0, 0, 1);
                            break;

                        case (Rarity.Epic):
                            gains += new PrimaryAttributes(3, 0, 0, 2);
                            break;

                        case (Rarity.Legendary):
                            gains += new PrimaryAttributes(4, 0, 0, 3);
                            break;

                    }
                    break;
            }
            return gains;
        }
    }


}
