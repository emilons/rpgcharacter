﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacter.Classes;
using RPGCharacter.Exceptions;


namespace RPGCharacter
{
    /// <summary>
    /// A character in the RPG (Role Playing Game).
    /// </summary>
    public abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }
        public SecondaryAttributes SecondaryAttributes { get; set; }
        public double CharacterDPS { get; set; } = 1.0;
        
        /// <summary>
        /// The possible main attributes used to calculate a character's damage.
        /// </summary>
        public enum Attributes
        {
            Strength,
            Dexterity,
            Intelligence
        }
        public Attributes DamageAttribute { get; set; }

        /// <summary>
        /// The item slots which a character can equip items on.
        /// </summary>
        public Dictionary<Item.Slot, Item> Equipment = new Dictionary<Item.Slot, Item>
        {
            {Item.Slot.Head, null },
            {Item.Slot.Body, null },
            {Item.Slot.Legs, null },
            {Item.Slot.Weapon, null },
        };

        /// <summary>
        /// The valid types of armor and weapons for a specific character profession.
        /// </summary>
        public List<Weapon.WeaponType> validWeapons;
        public List<Armor.ArmorType> validArmor;

        /// <summary>
        /// Increases the level of a character.
        /// </summary>
        /// <param name="lvl">The amount of levels to gain.</param>
        public abstract void LevelUp(int lvl);

        public override string ToString()
        {
            return $"Name: {Name}, Level: {Level}";
        }

        /// <summary>
        /// Displays the character information and attributes in the console.
        /// </summary>
        public void DisplayStats()
        {
            StringBuilder display = new StringBuilder();
            display.Append($"---------------------------\n");
            display.Append($"Character Name: {Name}\n");
            display.Append($"Character Class: {GetType().ToString().Split('.')[2]}\n");
            display.Append($"Character Level: {Level}\n");
            display.Append($"Attributes:\n");
            display.Append($"-----------\n");
            display.Append($"Strength: {TotalPrimaryAttributes.Strength}\n");
            display.Append($"Dexterity: {TotalPrimaryAttributes.Dexterity}\n");
            display.Append($"Intelligence: {TotalPrimaryAttributes.Intelligence}\n");
            display.Append($"Vitality: {TotalPrimaryAttributes.Vitality}\n");
            display.Append($"Health: {SecondaryAttributes.Health}\n");
            display.Append($"Armor Rating: {SecondaryAttributes.ArmorRating}\n");
            display.Append($"Elemental Resistance: {SecondaryAttributes.ElementalResistance}\n");
            display.Append($"---------------------------");
            Console.WriteLine(display.ToString());
        }

        /// <summary>
        /// Displays the gear a character has equipped in the console.
        /// </summary>
        public void DisplayGear()
        {
            StringBuilder display = new StringBuilder();
            display.Append($"---------------------------\n");
            display.Append($"Gear: \n");
            display.Append($"-----------\n");
            foreach (KeyValuePair<Item.Slot, Item> gear in Equipment)
            {
                Item.Slot slot = gear.Key;
                if (gear.Value != null)
                {
                    Item piece = gear.Value;
                    if (slot == Item.Slot.Weapon)
                    {
                        display.Append($"{slot}: {piece.Name} [lvl: {piece.Level}," +
                            $" DPS: {CharacterDPS.ToString("F").Replace(",", ".")}]\n");
                    }
                    else
                    {
                        display.Append($"{slot}: {piece.Name} [lvl: {piece.Level}]\n");
                    }
                }
                else
                {
                    display.Append($"{slot}: None\n");
                }
                
            }
            display.Append($"---------------------------");
            Console.WriteLine(display.ToString());
        }

        /// <summary>
        /// Equips a character with an item.
        /// </summary>
        /// <param name="item">The item to be equipped.</param>
        /// <returns>Returns a message that equipment of the item was succesfull.</returns>
        public string EquipItem(Item item)
        {
            // Handle equipping weapons
            if (item.InventorySlot == Item.Slot.Weapon)
            {
                EquipWeapon(item);
                UpdateCharacterDPS();
                return $"New weapon {item.Name} equipped!";
            }
            // Handle equipping armor
            else
            {
                EquipArmor(item);
                // Handle update of TotalPrimaryAttributes (only relevant for armor).
                UpdateTotalAttributes();
                return $"New armor {item.Name} equipped!";

            }
            
        }

        /// <summary>
        /// Helper function to EquipItem() for weapons.
        /// Throws InvalidWeaponException if weapon cannot be equipped.
        /// </summary>
        /// <param name="item">The weapon to be equipped.</param>
        private void EquipWeapon(Item item)
        {
            Weapon weapon = (Weapon)item;
            if (!validWeapons.Contains(weapon.Type))
            {
                // Accounting for grammar
                if (weapon.Type == Weapon.WeaponType.Axe)
                {
                    throw new InvalidWeaponException($"Your character cannot equip an {weapon.Type.ToString()}");
                }
                else
                {
                    throw new InvalidWeaponException($"Your character cannot equip a {weapon.Type.ToString()}");
                }
            }
            if (item.Level > Level)
            {
                string message = "Your character level is too low to wield this weapon";
                throw new InvalidWeaponException(message);
            }
            Equipment[item.InventorySlot] = item;

        }

        /// <summary>
        /// Helper function to EquipItem() for armor.
        /// Throws InvalidArmorException if armor cannot be equipped.
        /// </summary>
        /// <param name="item">The armor to be equipped.</param>
        private void EquipArmor(Item item)
        {
            Armor armor = (Armor)item;
                if (!validArmor.Contains(armor.Type))
                {
                    throw new InvalidArmorException($"Your character cannot equip {armor.Type.ToString()} {armor}");
                }
                if (item.Level > Level)
                {
                    throw new InvalidArmorException("Your character level is too low to wield this armor piece");
                }
            Equipment[item.InventorySlot] = item;
        }

        // Function for updating TotalPrimaryAttributes.
        /// <summary>
        /// Updates the total attributes of a character, 
        /// adding the base attributes with any additional attributes from gear.
        /// </summary>
        public void UpdateTotalAttributes()
        {
            PrimaryAttributes newTotal = BasePrimaryAttributes;
            foreach (KeyValuePair<Item.Slot, Item> gear in Equipment) {
                if (gear.Value == null)
                {
                    continue;
                }
                newTotal += new PrimaryAttributes(gear.Value.Attributes.Strength,
                    gear.Value.Attributes.Dexterity,
                    gear.Value.Attributes.Intelligence,
                    gear.Value.Attributes.Vitality);

            }
            TotalPrimaryAttributes = newTotal;
        }

        // Function for updating CharacterDPS.
        /// <summary>
        /// Updates the DPS of a character,
        /// using the weapon DPS and the character's damage attribute.
        /// </summary>
        public void UpdateCharacterDPS()
        {
            Item weaponSlot = Equipment[Item.Slot.Weapon];
            double newDPS = 1;
            // Null if character has no weapon equipped
            if (weaponSlot != null)
            {
                Weapon weapon = (Weapon)weaponSlot;
                switch (DamageAttribute)
                {
                    case (Attributes.Strength):
                        newDPS = weapon.DPS * (1 + TotalPrimaryAttributes.Strength / 100);
                        break;

                    case (Attributes.Dexterity):
                        newDPS = weapon.DPS * (1 + TotalPrimaryAttributes.Dexterity / 100);
                        break;

                    case (Attributes.Intelligence):
                        newDPS = weapon.DPS * (1 + TotalPrimaryAttributes.Intelligence / 100);
                        break;
                }
            }
            else 
            {
                switch (DamageAttribute)
                {
                    case (Attributes.Strength):
                        newDPS *= (1 + TotalPrimaryAttributes.Strength / 100);
                        break;

                    case (Attributes.Dexterity):
                        newDPS *= (1 + TotalPrimaryAttributes.Dexterity / 100);
                        break;

                    case (Attributes.Intelligence):
                        newDPS *= (1 + TotalPrimaryAttributes.Intelligence / 100);
                        break;
                }
            }
            CharacterDPS = newDPS;
            //Console.WriteLine($"New Character DPS is: {CharacterDPS.ToString("F").Replace(",",".")}");
        }
    }
}
