﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacter.Exceptions;

namespace RPGCharacter.Classes
{
    /// <summary>
    /// Object which characters can equip in order to increase their power.
    /// </summary>
    public abstract class Item
    {   
        public string Name { get; set; }
        public int Level { get; set; }
        public Slot InventorySlot { get; set; }
        public PrimaryAttributes Attributes { get; set; }

        public enum Slot
        {
            Head,
            Body,
            Legs,
            Weapon
        }

        public enum Rarity
        {
            Common,
            Uncommon,
            Rare,
            Epic,
            Legendary
        }


    }
}
