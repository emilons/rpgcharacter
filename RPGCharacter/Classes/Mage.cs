﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacter.Exceptions;

namespace RPGCharacter.Classes
{
    /// <summary>
    /// A type of character profession, 
    /// making use of their knowledge of the arcane and high intelligence to deal damage.
    /// </summary>
    public class Mage : Character
    {
        /// <summary>
        /// Creates a fresh level one mage.
        /// </summary>
        /// <param name="name">Name of the mage.</param>
        public Mage(string name)
        {
            Name = name;
            Level = 1;
            PrimaryAttributes prim = new PrimaryAttributes(1, 1, 8, 5);
            BasePrimaryAttributes = prim;
            TotalPrimaryAttributes = prim;
            SecondaryAttributes sec = new SecondaryAttributes(TotalPrimaryAttributes);
            SecondaryAttributes = sec;
            DamageAttribute = Attributes.Intelligence;
            UpdateCharacterDPS();
            validWeapons = new()
            {
                Weapon.WeaponType.Staff,
                Weapon.WeaponType.Wand
            };
            validArmor = new()
            {
                Armor.ArmorType.Cloth
            };

        }

        /// <summary>
        /// Increases the level of the mage.
        /// </summary>
        /// <param name="lvl">The amount of levels to gain.</param>
        public override void LevelUp(int lvl = 1)
        {

            if (lvl <= 0)
            {
                throw new ArgumentException("Must gain at least 1 Level");
            }
            Level += lvl;
            Console.WriteLine($"{Name} is now level {Level}!");
            BasePrimaryAttributes += new PrimaryAttributes(1 * lvl, 1 * lvl, 5 * lvl, 3 * lvl);
            UpdateTotalAttributes();
            SecondaryAttributes = new SecondaryAttributes(TotalPrimaryAttributes);
        }
    }
}
