﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacter.Classes
{
    /// <summary>
    /// The main attributes of a character or item. 
    /// </summary>
    public class PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; } 
        public int Intelligence { get; set; }
        public int Vitality { get; set; }
        
        /// <summary>
        /// Default constructor creating an empty object.
        /// </summary>
        public PrimaryAttributes()
        {
            Strength = 0;
            Dexterity = 0;
            Intelligence = 0;
            Vitality = 0;
        }

        /// <summary>
        /// Main constructor which sets the different attributes in the object.
        /// </summary>
        /// <param name="strength">The strength attribute.</param>
        /// <param name="dexterity">The dexterity attribute.</param>
        /// <param name="intelligence">The intelligence attribute.</param>
        /// <param name="vitality">The vitality attribute.</param>
        public PrimaryAttributes(int strength, int dexterity, int intelligence, int vitality)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
            Vitality = vitality;
        }

        
        /// <summary>
        /// Overloads the +-operator. Simplifies attribute addition, targeting the attributes 
        /// within the PrimaryAttributes object directly.
        /// </summary>
        /// <param name="left">Left hand side of the operation.</param>
        /// <param name="right">Right hand side of the operation.</param>
        /// <returns>The result of the attribute addition.</returns>
        public static PrimaryAttributes operator +(PrimaryAttributes left, PrimaryAttributes right)
        {
            return new PrimaryAttributes(left.Strength + right.Strength, 
                left.Dexterity + right.Dexterity, 
                left.Intelligence + right.Intelligence, 
                left.Vitality + right.Vitality);
        }

        public override bool Equals(object obj)
        {
            return obj is PrimaryAttributes attributes &&
                   Strength == attributes.Strength &&
                   Dexterity == attributes.Dexterity &&
                   Intelligence == attributes.Intelligence &&
                   Vitality == attributes.Vitality;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Strength, Dexterity, Intelligence, Vitality);
        }

        public override string ToString() 
        { 
            return $"Strength = {Strength}, " +
                $"Dexterity = {Dexterity}, " +
                $"Intelligence = {Intelligence}, " +
                $"Vitality = {Vitality}";
        }
    }
}
