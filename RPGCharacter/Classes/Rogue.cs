﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacter.Exceptions;

namespace RPGCharacter.Classes
{
    /// <summary>
    /// A type of character profession, 
    /// making use of their speed and stealth as well as high dexterity to deal damage.
    /// </summary>
    public class Rogue : Character
    {
        /// <summary>
        /// Creates a fresh level one rogue.
        /// </summary>
        /// <param name="name">Name of the rogue.</param>
        public Rogue(string name)
        {
            Name = name;
            Level = 1;
            PrimaryAttributes prim = new PrimaryAttributes(2, 6, 1, 8);
            BasePrimaryAttributes = prim;
            TotalPrimaryAttributes = prim;
            SecondaryAttributes sec = new SecondaryAttributes(TotalPrimaryAttributes);
            SecondaryAttributes = sec;
            DamageAttribute = Attributes.Dexterity;
            UpdateCharacterDPS();
            validWeapons = new()
            {
                Weapon.WeaponType.Dagger,
                Weapon.WeaponType.Sword
            };
            validArmor = new()
            {
                Armor.ArmorType.Leather,
                Armor.ArmorType.Mail
            };

        }

        /// <summary>
        /// Increases the level of the rogue.
        /// </summary>
        /// <param name="lvl">The amount of levels to gain.</param>
        public override void LevelUp(int lvl = 1)
        {
            if (lvl <= 0)
            {
                throw new ArgumentException("Must gain at least 1 Level");
            }
            Level += lvl;
            Console.WriteLine($"{Name} is now level {Level}!");
            BasePrimaryAttributes += new PrimaryAttributes(1 * lvl, 4 * lvl, 1 * lvl, 3 * lvl);
            UpdateTotalAttributes();
            SecondaryAttributes = new SecondaryAttributes(TotalPrimaryAttributes);
        }
    }
}
