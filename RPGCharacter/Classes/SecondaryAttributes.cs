﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacter.Classes
{
    /// <summary>
    /// The secondary attributes of a character. 
    /// </summary>
    public class SecondaryAttributes
    {
        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementalResistance { get; set; }

        /// <summary>
        /// Default constructor setting the attributes.
        /// </summary>
        /// <param name="total">The total primary attributes of a character.</param>
        public SecondaryAttributes(PrimaryAttributes total)
        {
            Health = total.Vitality * 10;
            ArmorRating = total.Strength + total.Dexterity;
            ElementalResistance = total.Intelligence;
        }

        /// <summary>
        /// Optional constructor setting the attributesv directly. Made for testing purposes.
        /// </summary>
        /// <param name="health">The health attribute.</param>
        /// <param name="armorRating">The armor rating attribute.</param>
        /// <param name="elementalResistance">The elemental resistance attribute.</param>
        public SecondaryAttributes(int health, int armorRating, int elementalResistance)
        {
            Health = health;
            ArmorRating = armorRating;
            ElementalResistance = elementalResistance;
        }

        public override bool Equals(object obj)
        {
            return obj is SecondaryAttributes attributes &&
                   Health == attributes.Health &&
                   ArmorRating == attributes.ArmorRating &&
                   ElementalResistance == attributes.ElementalResistance;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Health, ArmorRating, ElementalResistance);
        }

        public override string ToString()
        {
            return $"Health = {Health}, Armor Rating = {ArmorRating}, Elemental Resistance = {ElementalResistance}";
        }
    }

    

}
