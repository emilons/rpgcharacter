﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacter.Exceptions;

namespace RPGCharacter.Classes
{
    /// <summary>
    /// A type of character profession, 
    /// making use of raw force and high strength to deal damage.
    /// </summary>
    public class Warrior : Character
    {
        /// <summary>
        /// Creates a fresh level one warrior.
        /// </summary>
        /// <param name="name">Name of the warrior.</param>
        public Warrior (string name)
        {
            Name = name;
            Level = 1;
            PrimaryAttributes prim = new PrimaryAttributes(5, 2, 1, 10);
            BasePrimaryAttributes = prim;
            TotalPrimaryAttributes = prim;
            SecondaryAttributes sec = new SecondaryAttributes(TotalPrimaryAttributes);
            SecondaryAttributes = sec;
            DamageAttribute = Attributes.Strength;
            UpdateCharacterDPS();
            validWeapons = new()
            {
                Weapon.WeaponType.Axe,
                Weapon.WeaponType.Hammer,
                Weapon.WeaponType.Sword
            };
            validArmor = new()
            {
                Armor.ArmorType.Mail,
                Armor.ArmorType.Plate
            };
        }

        /// <summary>
        /// Increases the level of the warrior.
        /// </summary>
        /// <param name="lvl">The amount of levels to gain.</param>
        public override void LevelUp(int lvl = 1)
        {
            if (lvl <= 0)
            {
                throw new ArgumentException("Must gain at least 1 Level");
            }
            Level += lvl;
            Console.WriteLine($"{Name} is now level {Level}!");
            BasePrimaryAttributes += new PrimaryAttributes(3*lvl, 2*lvl, 1*lvl, 5*lvl);
            UpdateTotalAttributes();
            SecondaryAttributes = new SecondaryAttributes(TotalPrimaryAttributes);
        }
    }
}
