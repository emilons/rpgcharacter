﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RPGCharacter.Classes
{
    /// <summary>
    /// Type of item characters can equip which incereases their damage.
    /// </summary>
    public class Weapon : Item
    {
        public enum WeaponType 
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand
        }

        public int BaseDamage { get; set; }
        public double AttackSpeed { get; set; }
        public double DPS { get; set; }
        public WeaponType Type { get; set; }

        /// <summary>
        /// Creates a weapon object.
        /// </summary>
        /// <param name="type">Type of the weapon.</param>
        /// <param name="lvl">Level of the weapon.</param>
        /// <param name="rarity">Rarity of the weapon, defaulted to common.</param>
        public Weapon (WeaponType type, int lvl, Rarity rarity = Rarity.Common)
        {   
            Name = $"{rarity.ToString()} {type.ToString()}";
            Level = lvl;
            InventorySlot = Slot.Weapon;
            Attributes = new PrimaryAttributes();
            Type = type;
            SetWeaponAttributes(type, lvl, rarity);
            DPS = BaseDamage * AttackSpeed;
        }

        private void SetWeaponAttributes(WeaponType type, int lvl, Rarity rarity)
        {
            // Weapon level damage modifier.
            switch (type)
            {
                case WeaponType.Axe:
                    BaseDamage = 3 + lvl;
                    AttackSpeed = 1.2;
                    break;

                case WeaponType.Bow:
                    BaseDamage = 3 + lvl;
                    AttackSpeed = 1.4;
                    break;

                case WeaponType.Dagger:
                    BaseDamage = 1 + lvl;
                    AttackSpeed = 1.8;
                    break;

                case WeaponType.Hammer:
                    BaseDamage = 4 + lvl;
                    AttackSpeed = 0.9;
                    break;

                case WeaponType.Staff:
                    BaseDamage = 3 + lvl;
                    AttackSpeed = 1.1;
                    break;

                case WeaponType.Sword:
                    BaseDamage = 3 + lvl;
                    AttackSpeed = 1.5;
                    break;

                case WeaponType.Wand:
                    BaseDamage = 2 + lvl;
                    AttackSpeed = 2.0;
                    break;
            }

            // Weapon rarity damage modifier.
            switch (rarity)
            {
                case Rarity.Common:
                    BaseDamage += 1;
                    break;

                case Rarity.Uncommon:
                    BaseDamage += 2;
                    break;

                case Rarity.Rare:
                    BaseDamage += 4;
                    break;

                case Rarity.Epic:
                    BaseDamage += 7;
                    break;

                case Rarity.Legendary:
                    BaseDamage += 10;
                    break;
            }
        }
    }

    
}
