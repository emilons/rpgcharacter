﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacter.Classes;

namespace RPGCharacter.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() { }
        
        public InvalidWeaponException(string message) : base(message)
        {
        }
    }
}
