using System;
using Xunit;
using RPGCharacter.Classes;

namespace RPGCharacterTests
{
    public class CharacterTests
    {
        [Fact]
        // NOTE: Character is abstract so uses Warrior class instead
        public void NewCharacter_NewCharacterIsLevelOne_ShouldBeLevelOne()
        {
            Warrior testWarrior = new Warrior("Chad");

            int expected = 1;
           
            Assert.Equal(expected, testWarrior.Level);
        }

        [Fact]
        public void LevelUp_LevelUpOnce_ShouldBeOneLevelHigher()
        {
            Warrior testWarrior = new Warrior("Chad");
            testWarrior.LevelUp();
            
            var expected = 2;

            Assert.Equal(expected, testWarrior.Level);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_LevelUpZeroOrNegative_ShouldThrowArgumentException(int lvl)
        {
            Warrior testWarrior = new Warrior("Chad");

            Action expected = () => testWarrior.LevelUp(lvl);

            Assert.Throws<ArgumentException>(expected);
        }

        [Fact]
        public void NewWarrior_CreateNewWarrior_ShouldHaveDefaultAttributes()
        {
            Warrior testWarrior = new Warrior("Chad");

            var expected = new PrimaryAttributes(5, 2, 1, 10);

            Assert.Equal(expected, testWarrior.BasePrimaryAttributes);
        }

        [Fact]
        public void NewRanger_CreateNewRanger_ShouldHaveDefaultAttributes()
        {
            Ranger testRanger = new Ranger("Mike");

            var expected = new PrimaryAttributes(1, 7, 1, 8);

            Assert.Equal(expected, testRanger.BasePrimaryAttributes);
        }

        [Fact]
        public void NewRogue_CreateNewRogue_ShouldHaveDefaultAttributes()
        {
            Rogue testRogue = new Rogue("Chris");

            var expected = new PrimaryAttributes(2, 6, 1, 8);

            Assert.Equal(expected, testRogue.BasePrimaryAttributes);
        }

        [Fact]
        public void NewMage_CreateNewMage_ShouldHaveDefaultAttributes()
        {
            Mage testMage = new Mage("Simon");

            var expected = new PrimaryAttributes(1, 1, 8, 5);

            Assert.Equal(expected, testMage.BasePrimaryAttributes);
        }

        [Fact]
        public void LevelUpWarrior_ShouldIncreaseAttributesByDefaultAmount()
        {
            Warrior testWarrior = new Warrior("Chad");
            testWarrior.LevelUp();
            
            var expected = new PrimaryAttributes(5 + 3, 2 + 2, 1 + 1, 10 + 5);

            Assert.Equal(expected, testWarrior.BasePrimaryAttributes);
        }

        [Fact]
        public void LevelUpRanger_ShouldIncreaseAttributesByDefaultAmount()
        {
            Ranger testRanger = new Ranger("Mike");
            testRanger.LevelUp();
            
            var expected = new PrimaryAttributes(1 + 1, 7 + 5, 1 + 1, 8 + 2);

            Assert.Equal(expected, testRanger.BasePrimaryAttributes);
        }

        [Fact]
        public void LevelUpRogue_ShouldIncreaseAttributesByDefaultAmount()
        {
            Rogue testRogue = new Rogue("Chris");
            testRogue.LevelUp();
            
            var expected = new PrimaryAttributes(2 + 1, 6 + 4, 1 + 1, 8 + 3);

            Assert.Equal(expected, testRogue.BasePrimaryAttributes);
        }

        [Fact]
        public void LevelUpMage_ShouldIncreaseAttributesByDefaultAmount()
        {
            Mage testMage = new Mage("Simon");
            testMage.LevelUp();
            
            var expected = new PrimaryAttributes(1 + 1, 1 + 1, 8 + 5, 5 + 3);

            Assert.Equal(expected, testMage.BasePrimaryAttributes);
        }

        [Fact]
        public void LevelUpWarrior_ShouldIncreaseSecondaryAttributesByDefaultAmount()
        {
            Warrior testWarrior = new Warrior("Chad");
            testWarrior.LevelUp();
            
            var expected = new SecondaryAttributes(150, 12, 2);

            Assert.Equal(expected, testWarrior.SecondaryAttributes);
        }
        
    }
}
