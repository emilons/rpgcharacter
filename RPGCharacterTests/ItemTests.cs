﻿using System;
using Xunit;
using RPGCharacter.Classes;
using RPGCharacter.Exceptions;

namespace RPGCharacterTests
{
    public class ItemTests
    {
        [Fact]
        public void EquipItem_EquipHighLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            Weapon testAxe = new Weapon(Weapon.WeaponType.Axe, 1, Item.Rarity.Common);
            Warrior testWarrior = new Warrior("Chad");
            testAxe.Level = 2;

            Action expected = () => testWarrior.EquipItem(testAxe);

            Assert.Throws<InvalidWeaponException>(expected);
        }

        [Fact]
        public void EquipItem_EquipHighLevelArmor_ShouldThrowInvalidArmorException()
        {
            Armor testPlateBody = new Armor(Armor.ArmorType.Plate, 1, Item.Slot.Body);
            Warrior testWarrior = new Warrior("Chad");
            testPlateBody.Level = 2;

            Action expected = () => testWarrior.EquipItem(testPlateBody);

            Assert.Throws<InvalidArmorException>(expected);
        }

        [Fact]
        public void EquipItem_EquipIllegalWeapon_ShouldThrowInvalidWeaponException()
        {
            Weapon testBow = new Weapon(Weapon.WeaponType.Bow, 1, Item.Rarity.Common);
            Warrior testWarrior = new Warrior("Chad");
            Action expected = () => testWarrior.EquipItem(testBow);

            Assert.Throws<InvalidWeaponException>(expected);
        }

        [Fact]
        public void EquipItem_EquipIllegalArmor_ShouldThrowInvalidArmorException()
        {
            Armor testClothHead = new Armor(Armor.ArmorType.Cloth, 1, Item.Slot.Head);
            Warrior testWarrior = new Warrior("Chad");
            Action expected = () => testWarrior.EquipItem(testClothHead);

            Assert.Throws<InvalidArmorException>(expected);
        }

        [Fact]
        public void EquipItem_EquipLegalWeapon_ShouldReturnSuccessMessage()
        {
            Weapon testAxe = new Weapon(Weapon.WeaponType.Axe, 1, Item.Rarity.Common);
            Warrior testWarrior = new Warrior("Chad");
            string expected = testWarrior.EquipItem(testAxe);

            Assert.Equal(expected, $"New weapon {testAxe.Name} equipped!");
        }

        [Fact]
        public void EquipItem_EquipLegalArmor_ShouldReturnSuccessMessage()
        {
            Armor testPlateBody = new Armor(Armor.ArmorType.Plate, 1, Item.Slot.Body);
            Warrior testWarrior = new Warrior("Chad");
            string expected = testWarrior.EquipItem(testPlateBody);

            Assert.Equal(expected, $"New armor {testPlateBody.Name} equipped!");
        }

        [Fact]
        public void EquipItem_EquipArmorAttributesBonus_ShouldIncreaseTotalPrimaryAttributes()
        {
            Armor testPlateBody = new Armor(Armor.ArmorType.Plate, 1, Item.Slot.Body, Item.Rarity.Epic);
            Warrior testWarrior = new Warrior("Chad");
            testWarrior.EquipItem(testPlateBody);

            // Level 1 Warrior attributes
            PrimaryAttributes baseOfWarrior = new PrimaryAttributes(5, 2, 1, 10);
            
            // Epic Plate Body bonus attributes
            PrimaryAttributes bonusFromArmor = new PrimaryAttributes(3, 0, 0, 2);

            PrimaryAttributes expected = baseOfWarrior + bonusFromArmor;

            Assert.Equal(expected, testWarrior.TotalPrimaryAttributes);
        }

        [Fact]
        public void CharacterDPS_CalculateUnarmedDPS_ShouldBeOnlyCharacterMainDamageModifier()
        {
            Warrior testWarrior = new Warrior("Chad");

            var expected = 1 * (1 + (5 / 100));

            Assert.Equal(expected, testWarrior.CharacterDPS);
        }

        [Fact]
        public void CharacterDPS_CalculateArmedDPS_ShouldBeFullCharacterDPS()
        {
            Warrior testWarrior = new Warrior("Chad");
            Weapon testAxe = new Weapon(Weapon.WeaponType.Axe, 1, Item.Rarity.Rare);
            testWarrior.EquipItem(testAxe);

            // Rare Axe damage values
            int baseDamage = 3 + testAxe.Level + 4;
            double attackSpeed = 1.2;

            var expected = (baseDamage * attackSpeed) *(1 + (5 / 100));

            Assert.Equal(expected, testWarrior.CharacterDPS);
        }

        [Fact]
        public void CharacterDPS_CalculateArmedAndArmoredDPS_ShouldBeFullCharacterDPS()
        {
            Warrior testWarrior = new Warrior("Chad");
            Weapon testAxe = new Weapon(Weapon.WeaponType.Axe, 1, Item.Rarity.Rare);
            Armor testPlateBody = new Armor(Armor.ArmorType.Plate, 1, Item.Slot.Body, Item.Rarity.Epic);
            testWarrior.EquipItem(testAxe);
            testWarrior.EquipItem(testPlateBody);

            // Rare Axe damage values
            int baseDamage = 3 + testAxe.Level + 4;
            double attackSpeed = 1.2;

            // Epic Plate Body Strength modifier
            int bonusStrength = 3;

            var expected = (baseDamage * attackSpeed) * (1 + ((5+bonusStrength) / 100));

            Assert.Equal(expected, testWarrior.CharacterDPS);
        }

        // Character.DisplayCharacter & Character.DisplayGear are the only public methods without unit tests,
        // as these only write to console.
    }
}
